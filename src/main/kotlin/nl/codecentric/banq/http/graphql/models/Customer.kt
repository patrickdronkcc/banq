package nl.codecentric.banq.http.graphql.models

import com.expediagroup.graphql.annotations.GraphQLID
import graphql.schema.DataFetchingEnvironment
import nl.codecentric.banq.domain.bankaccount.FetchAllBankAccountsForCustomerId
import nl.codecentric.banq.http.graphql.Context
import nl.codecentric.banq.projections.bankaccount.BankAccountDTO
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import java.util.concurrent.CompletableFuture

class Customer(@GraphQLID val id: String, val username: String, val password: String) {
    fun bankAccounts(context: Context, environment: DataFetchingEnvironment): CompletableFuture<List<BankAccount>> {
        val id = environment.getSource<Customer>().id
        return context
                .queryGateway
                .query(FetchAllBankAccountsForCustomerId(id), MultipleInstancesResponseType(BankAccountDTO::class.java))
                .thenApply { accounts -> accounts.map { BankAccount(it.bankAccountId, it.customerId, it.balance) } }
    }
}
