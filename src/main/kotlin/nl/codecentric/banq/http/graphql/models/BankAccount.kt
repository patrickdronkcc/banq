package nl.codecentric.banq.http.graphql.models

import graphql.schema.DataFetchingEnvironment
import nl.codecentric.banq.domain.transaction.FetchAllTransactionsForBankAccountId
import nl.codecentric.banq.http.graphql.Context
import nl.codecentric.banq.projections.transaction.TransactionDTO
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import java.math.BigDecimal
import java.util.concurrent.CompletableFuture

class BankAccount(val bankAccountId: String, val customerId: String, var balance: BigDecimal) {
    fun transactions(context: Context, environment: DataFetchingEnvironment): CompletableFuture<List<Transaction>> {
        val bankAccountId = environment.getSource<BankAccount>().bankAccountId
        return context.queryGateway
                .query(FetchAllTransactionsForBankAccountId(bankAccountId), MultipleInstancesResponseType(TransactionDTO::class.java))
                .thenApply { transactions -> transactions.map { Transaction(it.transactionId, it.sourceBankAccountId, it.targetBankAccountId, it.amount, it.status, it.transactionType) } }
    }
}

