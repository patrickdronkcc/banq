package nl.codecentric.banq.http.rest.customer

import nl.codecentric.banq.domain.customer.FetchAllCustomersQuery
import nl.codecentric.banq.domain.customer.RegisterCustomerCommand
import nl.codecentric.banq.projections.customer.CustomerDTO
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import org.axonframework.queryhandling.QueryGateway
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

@RestController
class CustomerController(private val commandGateway: CommandGateway, private val queryGateway: QueryGateway) {
    @PostMapping(path = ["/register"])
    fun register(@RequestBody request: RegisterCustomerRequest): ResponseEntity<Unit> {
        this.commandGateway.sendAndWait<RegisterCustomerCommand>(
                RegisterCustomerCommand(request.id, request.username, request.password)
        )
        return ResponseEntity.accepted().build()
    }

    @GetMapping("/customers")
    fun getAllUsers(): MutableList<CustomerDTO>? {
        return queryGateway.query(FetchAllCustomersQuery(), MultipleInstancesResponseType(CustomerDTO::class.java)).join()
    }
}
