package nl.codecentric.banq.http.rest.transaction

import java.util.UUID
import java.util.concurrent.CompletableFuture
import nl.codecentric.banq.domain.transaction.FetchAllTransactionsForBankAccountId
import nl.codecentric.banq.projections.transaction.TransactionDTO
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import org.axonframework.queryhandling.QueryGateway
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class TransactionController(private val queryGateway: QueryGateway) {
    @GetMapping("/transactions/{bankAccountId}")
    fun getTransactionsForCustomer(@PathVariable("bankAccountId") bankAccountId: String): CompletableFuture<MutableList<TransactionDTO>>? {
        return queryGateway.query(FetchAllTransactionsForBankAccountId(bankAccountId), MultipleInstancesResponseType(TransactionDTO::class.java))
    }
}
