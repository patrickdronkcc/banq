package nl.codecentric.banq.http.rest.bankaccount

import java.math.BigDecimal
import java.util.UUID

data class DepositRequest(val bankAccountId: String, val amount: BigDecimal)
data class WithdrawRequest(val bankAccountId: String, val amount: BigDecimal)
data class TransferRequest(
    val bankAccountId: String,
    val targetBankAccountId: String,
    val transactionId: String,
    val amount: BigDecimal
)
