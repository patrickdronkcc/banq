package nl.codecentric.banq.http.rest.bankaccount

import nl.codecentric.banq.domain.bankaccount.DepositCommand
import nl.codecentric.banq.domain.bankaccount.FetchAllBankAccountsForCustomerId
import nl.codecentric.banq.domain.bankaccount.FetchBankAccountById
import nl.codecentric.banq.domain.bankaccount.WithdrawCommand
import nl.codecentric.banq.domain.transaction.TransferCommand
import nl.codecentric.banq.projections.bankaccount.BankAccountDTO
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.InstanceResponseType
import org.axonframework.messaging.responsetypes.MultipleInstancesResponseType
import org.axonframework.queryhandling.QueryGateway
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.CompletableFuture

@RestController
class BankAccountController(
    private val queryGateway: QueryGateway,
    private val commandGateway: CommandGateway
) {
    @GetMapping("/bankaccounts/{customerId}")
    fun getAllUsers(@PathVariable("customerId") customerId: String): CompletableFuture<MutableList<BankAccountDTO>> {
        return queryGateway.query(FetchAllBankAccountsForCustomerId(customerId), MultipleInstancesResponseType(BankAccountDTO::class.java))
    }

    @GetMapping("/bankaccount/{bankAccountId}")
    fun getBankAccountById(@PathVariable("bankAccountId") bankAccountId: String): CompletableFuture<BankAccountDTO?> {
        return queryGateway.query(FetchBankAccountById(bankAccountId), InstanceResponseType(BankAccountDTO::class.java))
    }

    @PostMapping("/bankaccount/deposit")
    fun deposit(@RequestBody request: DepositRequest) {
        commandGateway.sendAndWait<DepositCommand>(DepositCommand(request.bankAccountId, request.amount))
    }

    @PostMapping("/bankaccount/withdraw")
    fun withdraw(@RequestBody request: WithdrawRequest) {
        commandGateway.sendAndWait<WithdrawCommand>(WithdrawCommand(request.bankAccountId, request.amount))
    }

    @PostMapping("/bankaccount/transfer")
    fun transfer(@RequestBody request: TransferRequest) {
        commandGateway.sendAndWait<TransferCommand>(
                TransferCommand(
                        request.transactionId,
                        request.bankAccountId,
                        request.targetBankAccountId,
                        request.amount
                )
        )
    }

}
