package nl.codecentric.banq.configuration

import com.mongodb.MongoClient
import org.axonframework.eventhandling.tokenstore.TokenStore
import org.axonframework.extensions.mongo.DefaultMongoTemplate
import org.axonframework.extensions.mongo.eventhandling.saga.repository.MongoSagaStore
import org.axonframework.extensions.mongo.eventsourcing.tokenstore.MongoTokenStore
import org.axonframework.serialization.Serializer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AxonConfig {
    @Bean
    fun defaultMongoTemplate(mongoClient: MongoClient): DefaultMongoTemplate {
        return DefaultMongoTemplate
                .builder()
                .mongoDatabase(mongoClient, "banq_db")
                .build()
    }

    @Bean
    fun tokenStore(defaultMongoTemplate: DefaultMongoTemplate, defaultSerializer: Serializer): TokenStore {
        return MongoTokenStore
                .builder()
                .mongoTemplate(defaultMongoTemplate)
                .serializer(defaultSerializer)
                .build()
    }

    @Bean
    fun sagaStore(defaultMongoTemplate: DefaultMongoTemplate, defaultSerializer: Serializer): MongoSagaStore {
        return MongoSagaStore
                .builder()
                .mongoTemplate(defaultMongoTemplate)
                .serializer(defaultSerializer)
                .build()
    }
}
