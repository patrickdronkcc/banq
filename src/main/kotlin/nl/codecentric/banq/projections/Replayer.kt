package nl.codecentric.banq.projections

import org.axonframework.config.EventProcessingConfiguration
import org.axonframework.eventhandling.TrackingEventProcessor
import org.springframework.stereotype.Component

@Component
class Replayer(private val eventProcessingConfiguration: EventProcessingConfiguration) {
    fun replay(processingGroup: String) {
        eventProcessingConfiguration.eventProcessor(processingGroup, TrackingEventProcessor::class.java)
                .ifPresent {
                    it.shutDown()
                    it.resetTokens()
                    it.start()
                }
    }
}
