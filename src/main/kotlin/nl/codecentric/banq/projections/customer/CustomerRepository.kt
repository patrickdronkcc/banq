package nl.codecentric.banq.projections.customer

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository

@Repository
interface CustomerRepository : MongoRepository<CustomerDTO, String> {
    fun findByUsername(username: String): CustomerDTO?
}
