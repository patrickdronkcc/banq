package nl.codecentric.banq.projections.customer

import nl.codecentric.banq.domain.customer.CustomerRegisteredEvent
import nl.codecentric.banq.domain.customer.FetchAllCustomersQuery
import org.axonframework.config.ProcessingGroup
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.stereotype.Component

@Component
@ProcessingGroup("CustomerProjection")
class CustomerProjection(private val customerRepository: CustomerRepository) {
    @EventHandler
    fun on(event: CustomerRegisteredEvent) {
        customerRepository.save(CustomerDTO(event.id, event.username, event.password))
    }

    @QueryHandler
    fun handle(query: FetchAllCustomersQuery): List<CustomerDTO> {
        return customerRepository.findAll()
    }
}
