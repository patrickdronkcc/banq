package nl.codecentric.banq.projections.transaction

import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface TransactionRepository : MongoRepository<TransactionDTO, String> {
    fun findAllBySourceBankAccountIdOrTargetBankAccountId(sourceBankAccountId: String, targetBankAccountId: String): List<TransactionDTO>
}
