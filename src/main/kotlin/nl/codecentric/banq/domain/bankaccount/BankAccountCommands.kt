package nl.codecentric.banq.domain.bankaccount

import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.math.BigDecimal

data class CreateBankAccountCommand(
    @TargetAggregateIdentifier
    val id: String,
    val customerId: String
)

data class DepositCommand(
    @TargetAggregateIdentifier
    val bankAccountId: String,
    val amount: BigDecimal
)

data class WithdrawCommand(
    @TargetAggregateIdentifier
    val bankAccountId: String,
    val amount: BigDecimal
)

data class CreditCommand(
    @TargetAggregateIdentifier
    val bankAccountId: String,
    val transactionId: String,
    val amount: BigDecimal
)

data class DebitCommand(
    @TargetAggregateIdentifier
    val bankAccountId: String,
    val transactionId: String,
    val amount: BigDecimal
)
