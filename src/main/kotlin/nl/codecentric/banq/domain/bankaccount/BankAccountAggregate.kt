package nl.codecentric.banq.domain.bankaccount

import java.math.BigDecimal
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate

@Aggregate
class BankAccountAggregate {
    @AggregateIdentifier
    lateinit var id: String
    lateinit var balance: BigDecimal

    // CommandHandlers
    @CommandHandler
    constructor(command: CreateBankAccountCommand) {
        AggregateLifecycle.apply(
                BankAccountCreatedEvent(command.id, command.customerId)
        )
    }

    @CommandHandler
    fun on(command: DepositCommand) {
        if (command.amount < BigDecimal.ZERO) {
            throw Exception("Deposit amount is lower then 0")
        }
        AggregateLifecycle.apply(DepositedEvent(command.bankAccountId, command.amount))
    }

    @CommandHandler
    fun on(command: WithdrawCommand) {
        throwIfBalanceIsInsufficient(command.amount)
        AggregateLifecycle.apply(WithdrawalEvent(command.bankAccountId, command.amount))
    }

    @CommandHandler
    fun on(command: CreditCommand) {
        throwIfBalanceIsInsufficient(command.amount)
        AggregateLifecycle.apply(CreditedEvent(this.id, command.transactionId, command.amount))
    }

    @CommandHandler
    fun on(command: DebitCommand) {
        AggregateLifecycle.apply(DebitedEvent(this.id, command.transactionId, command.amount))
    }

    // EventHandlers
    @EventSourcingHandler
    fun on(event: BankAccountCreatedEvent) {
        this.id = event.bankAccountId
        this.balance = BigDecimal.ZERO
    }

    @EventSourcingHandler
    fun on(event: DepositedEvent) {
        addToBalance(event.amount)
    }

    @EventSourcingHandler
    fun on(event: WithdrawalEvent) {
        deductFromBalance(event.amount)
    }

    @EventSourcingHandler
    fun on(event: CreditedEvent) {
        addToBalance(event.amount)
    }

    @EventSourcingHandler
    fun on(event: DebitedEvent) {
        deductFromBalance(event.amount)
    }

    // HelperFunctions
    private fun throwIfBalanceIsInsufficient(amount: BigDecimal) {
        if (this.balance < amount) {
            throw Exception("Balance not sufficient")
        }
    }

    private fun deductFromBalance(amount: BigDecimal) {
        this.balance -= amount
    }

    private fun addToBalance(amount: BigDecimal) {
        this.balance += amount
    }
}
