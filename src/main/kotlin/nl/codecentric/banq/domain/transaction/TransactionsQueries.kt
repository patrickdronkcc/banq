package nl.codecentric.banq.domain.transaction

import java.util.UUID

data class FetchAllTransactionsForBankAccountId(val bankAccountId: String)
