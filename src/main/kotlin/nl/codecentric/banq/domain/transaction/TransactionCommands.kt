package nl.codecentric.banq.domain.transaction

import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.math.BigDecimal

data class TransferCommand(
    @TargetAggregateIdentifier
    val transactionId: String,
    val sourceBankAccountId: String,
    val targetBankAccountId: String,
    val amount: BigDecimal
)

data class CompleteTransactionCommand(
    @TargetAggregateIdentifier
    val transactionId: String
)

data class CancelTransactionCommand(
    @TargetAggregateIdentifier
    val transactionId: String,
    val reason: String?
)
