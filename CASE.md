Case: Banq

You are working at a big national bank. The bank you are working for is noticing that the banking market is changing, 
a lot of disruptive startups are popping up. The bank decides to create a new startup called Banq, to boost innovation.

They are looking to build a minimal viable product (MVP). This was the accompanying text describing the functionality:

A customer can register at Banq, when the registration is successful, so after the manual check of the passport info,
 a bank account will be created for them. 

From this point on they can do several things. 
The customer can make a deposit, but only if it's less than 5000, because if it exceeds the threshold additional taxes 
need to be paid, and that’s out of scope. This is not the case for transactions.

They can also make withdrawals, but only if the balance is sufficient.

A customer can also start a transaction to other bank accounts, but again the balance needs to be sufficient. 
It can also occur that the other bank account is unknown, in this case the transaction needs to be cancelled. 
It can also happen that other banks are not so resilient on high traffic. 
So, when an external bank doesn’t react within 10 minutes, the transaction will be cancelled as well. 
We would like to try it twice. 

As the last addition they also let you know that a customer can create a notification on the balance of their bank account. 
When it gets below a certain amount the bank sends out an email to the customer.
