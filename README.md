# Axon Banq
This is the backend for the Banq. 
It's an Axon application with GraphQL as access layer to the Command and Query bus

 
## Running the Banq
First run the application by running `docker-compose up` from the root directory. 
It will create two services. First, it will create Axon server. Second, MongoDB for all our projections.
You can access Axon-server at [localhost:8024](http://localhost:8024)
You can access the MongoDB at localhost:27017 with username `mongo` and password `mongo`.
